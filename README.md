# Pd stuff
GPL version 3 or later  
(c) Pelle Hjek

## Puredata patches

- [**list**](lib/list)
  - Functional programming-ish hacks for list processing.
- [**harmony**](lib/harmony)
  - Stuff to do with western tonal music theory.
- [**rhythm**](lib/rhythm)
  - Any patches doing anything with rhythm.
- [**bingo**](lib/bingo)
  - Patch for organising Harsh Noise Bingo Battle events.

